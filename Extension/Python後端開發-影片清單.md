Python 後端開發 - 影片清單
======


【先讓程式能動】
------

### Web :
+ BootStrap : https://youtu.be/ctr6eo9dL-A

```
網頁前端框架，可以弄一個作品集
```

### Python : 
+ Python Flask : https://youtu.be/AiUzsr5JZgQ
+ Python GUI - WxPython : https://youtu.be/Hjg_qHPZ1-Q

```
也是為了弄作品集，等講到 Python 在看即可
```

【再讓程式容易修改】
------
1. 軟體設計概念 - 軟體的本質 : https://youtu.be/njAgG9e1Qco
2. 物件導向程式設計 - SOLID : https://youtu.be/dGbzlsBuw9M
3. 元件耦合性 3 大原則 : https://youtu.be/cSQY0rA7FPU
4. 元件內聚性 3 大原則 : https://youtu.be/wRyzCktcbNQ
5. DDD 領域驅動設計 : https://youtu.be/mducM_tUUEY


【軟體工程師的軟技能】
------
+ 工程師的問題解構流程 : https://youtu.be/z81gQlGIZqA
+ 軟技能 : 十部學習法 : https://youtu.be/Ya7XE47gPBo
+ 刻意練習 : 心智表徵 : https://youtu.be/7GR2CgH7RxA


【Git : 版本控制工具 】
------
1. git clone 專案: https://youtu.be/QWVQF9UvsDo
2. markdown viewer 安裝: https://youtu.be/0WzOzNEu8ws
3. git 基礎教程 #1 : https://youtu.be/MeQWrGrTfOw
4. git 基礎教程 #2 : https://youtu.be/5ff9ujdK0IY
5. git 基礎教程 #3 : https://youtu.be/vMCkYWM0k3Y
6. git flow 開發流程 : https://youtu.be/B1ZANddOHyM
7. git commit 訊息格式 : https://youtu.be/5DhWqmDP-3U

影片清單 : 說明
------
四個大主題，前後順序沒有影響，

主題內，加字符號同樣順序不會影響，但若為數字符號則有影響為觀看順序。


### 個人建議，前三個影片可以先看 :
1. 工程師的問題解構流程 : <https://youtu.be/z81gQlGIZqA>
2. 軟體設計概念 - 軟體的本質 : <https://youtu.be/njAgG9e1Qco>
3. 物件導向程式設計 - SOLID : <https://youtu.be/dGbzlsBuw9M>

Web - Bootstrap 跟 Python GUI - wxPython ，則根據需求選擇性觀看。
