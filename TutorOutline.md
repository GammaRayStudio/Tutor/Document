Gamma Ray 軟體工作室 , 程式教學大綱
======


科目
------
+ Web 前端入門 : 建置個人的形象網站  
+ Python 後端開發 : 成為 Python 後端工程師
+ Spring Boot + Vue : 前後端分離，Java 全端實戰開發
+ Android App 開發 : API 串接實務
+ iOS App 開發 : Swift + Swift UI 超入門


Web 前端入門 : 建置個人的形象網站  
------
1. 網路基本概念
2. Html
3. jQuery
4. Bootstrap
5. GitHub

<br>

Python 後端開發 : 成為 Python 後端工程師
------
### 前端
1. 網路基本概念
2. Html
3. jQuery

### 後端
+ Python 程式設計
  + Python 概論
  + 從 REPL 到 IDE
  + 基礎語法
  + 流程控制
  + 函式運用
  + 模組管理
  + 類別物件 `物件導向`
  + 例外處理
  + 檔案 I/O
+ 資料庫 SQL 
  + SQL 概述
  + SQL 命令
  + 關聯式資料庫
  + 資料庫正規劃
  + SQL 數據類型
  + SQL 運算符
  + SQL 基本語法
  + SQL 進階語法
+ Python Flask 框架 `編輯中`
+ Python Django 框架 `編輯中`

<br>

Spring Boot + Vue : 前後端分離，Java 全端工程師
------
### 基礎
+ Java 程式設計
  + 平台概論
  + 從 JDK 到 IDE
  + 基本語法
  + 流程控制
  + 方法(Method)
  + 類別與物件`物件導向`
  + 例外處理 `編輯中`
  + 檔案 I/O `編輯中`

### 前端
+ 網路基本概念
+ Html
+ jQuery

### 框架
+ Spring Boot `編輯中`
+ Vue `編輯中`

### 實作
`編輯中`
+ Spring Boot + Vue 整合 `專題`

<br>

Android App 開發 : API 串接實務
------
`編輯中`

+ Python 程式入門
  + Python 概論
  + 從 REPL 到 IDE
  + 基礎語法
  + 流程控制
  + 函式運用
+ Java 程式設計
+ Android App 開發 : Java 實作
+ Python 簡易伺服器架設 
  + Python Flask 框架
  + Python SQL 實作
+ Android 前後端 API 串接整合

<br>

iOS App 開發 : Swift + Swift UI 超入門
------
`編輯中`

+ Swift 程式設計
+ iOS App 開發 : SwiftUI 實作







