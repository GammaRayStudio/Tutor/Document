Swift : iOS + SwiftUI 課程大綱
======
`開發工具 + 程式概念 + Swift 語言 + SwiftUI 框架 + iOS 開發`

+ [git clone](https://gitlab.com/GammaRayStudio/Tutor/Document)


開發工具
------
+ Mac 
+ XCode `App Store`
+ VSCode `Swift`
+ Git
+ SourceTree


<br>

基礎概念
------
`電腦科學`

+ 電腦概論
+ 程式概念


<br>

Swift 程式語言
------
`Swift vs Object-C`

+ Swift 基礎概論
+ 基本語法
+ 控制流程
+ 定義方法
+ 定義類別
+ 物件導向
+ 輸入輸出
+ 例外處理


<br>

SwiftUI 框架
------
`參考 : Mark Account 實作內容`

+ [編輯中...]

<br>

iOS 開發
------
`Swift 語法 + SwiftUI 框架`

+ [編輯中...]


參考資料
------
### 《The Swift Programming Language》`官方文件`
+ <https://www.swift.org/documentation/>

### 《The Swift Programming Language》中文版
+ <https://swiftgg.gitbook.io/swift>

### 《Hacking with Swift》
<https://www.hackingwithswift.com/read>



