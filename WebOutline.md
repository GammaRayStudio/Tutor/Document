Web 課程大綱
======
`開發工具 + 網路概念 + Html + jQuery + Bootstrap`

+ [git clone](https://gitlab.com/GammaRayStudio/Tutor/Document)

<br>

開發工具 `Tools`
------
+ Git : GitLab & GitHub
+ Web Server : Tomcat
+ Web IDE : Brackets

<br>

### Git 
`版本控制`

+ Git
+ SourceTree
+ Chrome Extension - Markdown Viewer

**document**
+ <https://gitlab.com/GammaRayStudio/DevDoc.git>

```
1. Git/001-1.git-basic-oper-part-I.md
2. Markdown/002.markdown-tutor.md
```

**application**
+ 文件與原始碼取得方法
+ 求職面試加分項目，[GitHub Pages 作品集] 線上展示
+ 一定水準的資訊公司都會用，之後入職也得學

[GitHub Pages 作品集]:https://gamma-ray-studio.github.io/zh-cht/index.html

<br>

### Web Server
+ Tomcat Server
+ XAMPP : Apache

**document**
+ <https://gitlab.com/GammaRayStudio/Tutor/WebDoc>

```
WebDoc/Tomcat.md
```

<br>

### Brackets
+ Plugin
+ Feature
+ VSCode

**document**
+ <https://gitlab.com/GammaRayStudio/Tutor/WebDoc>

```
WebDoc/Bracket.md
```

<br>

網路基礎概念
------
+ 網路的本質 ?
+ 什麼是 HTTP ? 
+ 什麼是 TCP/IP ?
+ 什麼是 API ?
+ 常見資料格式
+ 網路交換資料格式 : SOAP
+ 網路交換資料格式 : 其他

**document**
+ <https://gitlab.com/GammaRayStudio/Tutor/WebDoc>

```
WebDoc/Network.md
```

<br>

網頁設計 `Web`
------
+ Html
+ jQuery
+ Bootstrap

### Html
`80/20 基本語法`

+ 概念與應用
+ 標準樣板
+ 標籤與元素
+ 常用標籤元素
+ 屬性功能
+ 文字格式
+ CSS 用法
+ 表單提交
+ 參考資料

**document**
+ <https://gitlab.com/GammaRayStudio/Tutor/WebDoc>

```
WebDoc/Html.md
```

**Sample Code**
+ https://gitlab.com/GammaRayStudio/Tutor/WebSE

<br>

### jQuery
`主軸`

+ javascript 關係
+ jQuery 用法
+ Sample 01 : Text + Button + Event
+ Sample 02 : F12 + Console
+ Sample 03 : Table & List
+ jQuery UI
+ Moment
+ DataTable
+ Ajax

**document**
+ <https://gitlab.com/GammaRayStudio/Tutor/WebDoc>

```
WebDoc/jQuery.md
```

**Sample Code**
+ https://gitlab.com/GammaRayStudio/Tutor/WebSE

<br>

### Bootstrap
`會用`

**document**
+ <https://gitlab.com/GammaRayStudio/DevDoc.git>

```
Web/001.BootstrapSE.md
```



