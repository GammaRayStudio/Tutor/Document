前後端 開發 : Python + Web
======
`前端 : Html + jQuery / 後端 : Python - Flask、Django`

+ [git clone](https://gitlab.com/GammaRayStudio/Tutor/Document)

<br>

課程大綱
------
+ 開發工具
+ 前端
  + 網路概念
  + Html
  + jQuery
+ 後端
  + Python 概論
  + Python 程式語法
  + Python - Flask 框架
  + 資料庫 - SQL 語法 
  + Python - Django 框架

<br>

開發工具 `Tools`
------
+ Git : <https://git-scm.com/>
+ SourceTree : <https://www.sourcetreeapp.com/>
+ XAMPP : <https://www.apachefriends.org/download.html>
+ Brackets : <https://github.com/adobe/brackets/releases/tag/release-1.14.2>
+ VSCode : <https://code.visualstudio.com/download>

<br>

### 開發工具 : [版本控制]
`取得課程講義與範例程式碼`

[版本控制]:https://zh.wikipedia.org/wiki/%E7%89%88%E6%9C%AC%E6%8E%A7%E5%88%B6

+ Git 
+ SourceTree
+ Chrome Extension - [Markdown Viewer]


[Markdown Viewer]:https://chrome.google.com/webstore/detail/markdown-viewer/ckkdlimhmcjmikdlpkmbgfkaikojcbjk?utm_source=chrome-ntp-icon

<br>

**application**
+ 專案的程式碼，版本追蹤與管控
+ 求職面試加分項目，[GitHub Pages 作品集] 線上展示
+ 一定水準的資訊公司都會用，之後入職也得學

[GitHub Pages 作品集]:https://gamma-ray-studio.github.io/zh-cht/index.html

<br>

**video**
+ git clone 專案: <https://youtu.be/QWVQF9UvsDo>
+ markdown viewer 安裝: <https://youtu.be/0WzOzNEu8ws>
+ git 基礎教程 #1 : <https://youtu.be/MeQWrGrTfOw>
+ git 基礎教程 #2 : <https://youtu.be/5ff9ujdK0IY>
+ git 基礎教程 #3 : <https://youtu.be/vMCkYWM0k3Y>
+ git flow 開發流程 : <https://youtu.be/B1ZANddOHyM>
+ git commit 訊息格式 : <https://youtu.be/5DhWqmDP-3U>

<br>

**document**
+ <https://gitlab.com/GammaRayStudio/DevDoc.git>

```
1. Git/001-1.git-basic-oper-part-I.md
2. Markdown/002.markdown-tutor.md
```

<br>

### Web Server
`XAMPP : Apache`
+ [LAMP] 
  + Linux
  + Apache
  + MariaDB / MySQL
  + PHP / Perl / Python

[LAMP]:https://zh.wikipedia.org/wiki/LAMP

**document**
+ <https://gitlab.com/GammaRayStudio/Tutor/WebDoc>
+ <https://gitlab.com/GammaRayStudio/Tutor/PythonDoc>

```
WebDoc/XAMPP.md
PythonDoc/Flask.md
```

<br>

### Brackets
+ Plugin
+ Feature
+ VSCode

**document**
+ <https://gitlab.com/GammaRayStudio/Tutor/WebDoc>

```
WebDoc/Bracket.md
```

<br>

網路基礎概念
------
+ 網路的本質 ?
+ 什麼是 HTTP ? 
+ 什麼是 TCP/IP ?
+ 什麼是 API ?
+ 常見資料格式
+ 網路交換資料格式 : SOAP
+ 網路交換資料格式 : 其他

**document**
+ <https://gitlab.com/GammaRayStudio/Tutor/WebDoc>

```
WebDoc/Network.md
```

<br>

網頁設計 `Web`
------
+ Html
+ jQuery

### Html
`80/20 基本語法`

+ 概念與應用
+ 標準樣板
+ 標籤與元素
+ 常用標籤元素
+ 屬性功能
+ 文字格式
+ CSS 用法
+ 表單提交
+ 參考資料

**document**
+ <https://gitlab.com/GammaRayStudio/Tutor/WebDoc>

```
WebDoc/Html.md
```

**Sample Code**
+ https://gitlab.com/GammaRayStudio/Tutor/WebSE

<br>

### jQuery
+ javascript 關係
+ jQuery 用法
+ Sample 01 : Text + Button + Event
+ Sample 02 : F12 + Console
+ Sample 03 : Table & List
+ jQuery UI
+ Moment
+ DataTable
+ Ajax

**document**
+ <https://gitlab.com/GammaRayStudio/Tutor/WebDoc>

```
WebDoc/jQuery.md
```

**Sample Code**
+ https://gitlab.com/GammaRayStudio/Tutor/WebSE

<br>


Python 後端
------
### Python 程式語言
+ [PythonOutline.md](PythonOutline.md)

**document**
+ <https://gitlab.com/GammaRayStudio/Tutor/PythonDoc>

**Sample Code**
+ <https://gitlab.com/GammaRayStudio/Tutor/PythonSE>

<br>

### Python - Flask 框架
`快速了解 Python 網頁框架`

+ [編輯中...]

<br>

**Python Flask 入門指南**
+ [影片](https://youtu.be/AiUzsr5JZgQ)
+ [文章](https://gitlab.com/GammaRayStudio/DevDoc/-/blob/master/Python/002.PythonFlask.md)

<br>

### 資料庫 SQL 語法
+ [SQL-Outline.md](SQL-Outline.md)

**document**
+ <https://gitlab.com/GammaRayStudio/Tutor/SQL-DOC>

<br>

### Python - Django 框架
`業界使用的 Python 網頁框架`

+ [編輯中...]

















