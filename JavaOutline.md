Java 課程大綱
------
`開發工具 + Java 概論 + 程式語法 `

+ [git clone](https://gitlab.com/GammaRayStudio/Tutor/Document)

開發工具 `Tools`
------
+ Git : https://www.sourcetreeapp.com/
+ SourceTree : https://www.sourcetreeapp.com/
+ IntelliJ : https://www.jetbrains.com/idea/download/

<br>

### 取得課程講義與範例程式碼

+ Git 
+ SourceTree
+ Chrome Extension - [Markdown Viewer]

[Markdown Viewer]:https://chrome.google.com/webstore/detail/markdown-viewer/ckkdlimhmcjmikdlpkmbgfkaikojcbjk?utm_source=chrome-ntp-icon

**video**
+ git clone 專案: <https://youtu.be/QWVQF9UvsDo>
+ markdown viewer 安裝: <https://youtu.be/0WzOzNEu8ws>
+ git 基礎教程 #1 : <https://youtu.be/MeQWrGrTfOw>
+ git 基礎教程 #2 : <https://youtu.be/5ff9ujdK0IY>
+ git 基礎教程 #3 : <https://youtu.be/vMCkYWM0k3Y>
+ git flow 開發流程 : <https://youtu.be/B1ZANddOHyM>
+ git commit 訊息格式 : <https://youtu.be/5DhWqmDP-3U>

**document**
+ <https://gitlab.com/GammaRayStudio/DevDoc.git>

```
1. Git/001-1.git-basic-oper-part-I.md
2. Markdown/002.markdown-tutor.md
```

**application**
+ 文件與原始碼取得方法
+ 求職面試加分項目，[GitHub Pages 作品集] 線上展示
+ 一定水準的資訊公司都會用，之後入職也得學

[GitHub Pages 作品集]:https://gamma-ray-studio.github.io/zh-cht/index.html

<br>

### Java 開發工具
`IntelliJ`

    說明完 Java 概論後，接著語法說明之前，
    會說明 IntelliJ 的用法 與 一般專案和 Gradle 專案的差異

<br>



Java 概論
------
+ Java 前世今生
+ 三大平台 : Java SE / Java EE / Java ME
+ JVM / JRE / JDK
+ JDK : Java 開發套件
+ JavaCLI : Hello World
+ JavaCLI : Class
+ JavaCLI : Package
+ Java IDE : IntelliJ


**document**
+ https://gitlab.com/GammaRayStudio/Tutor/JavaDoc

```
1. 001.Java-Overview.md
2. 002.JDK-IDE.md
3. IntelliJ.md
```

**sample code**
+ https://gitlab.com/GammaRayStudio/Tutor/JavaSE

```
1. JavaCLI/Hello World
2. JavaCLI/Class
3. JavaCLI/Package
```

Java 程式語法
------
+ 基本語法
+ 控制流程
+ 函式方法
+ [編輯中...]


**document**
+ https://gitlab.com/GammaRayStudio/Tutor/JavaDoc

```
1. 003.basic-syntax.md
2. 004.flow-control.md
3. 005.method.md
```

**sample code**
+ https://gitlab.com/GammaRayStudio/Tutor/JavaSE

```
JavaProjSE : com/enoxs/basic/
1. J1_BasicSyntax.java
2. J2_FlowControl.java
3. J3_Method.java
```






