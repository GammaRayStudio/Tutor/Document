Python 課程大綱
======


Python 概論
------
+ 認識 Python
  + Python FAQ
  + Python 版本
  + Python 社群
  + Python 環境
  + Python 實作
  + Python 禪學
+ 從 REPL 到 IDE
  + REPL
  + Python 程式碼
  + Python IDE


Python 程式語法
------
+ 基本語法
+ 控制流程
+ 定義函式
+ 模組管理
+ 類別物件
+ 物件導向
+ 例外處理
+ 輸入輸出

<br>

**document**
+ <https://gitlab.com/GammaRayStudio/Tutor/PythonDoc>

**Sample Code**
+ <https://gitlab.com/GammaRayStudio/Tutor/PythonSE>

<br>

[TutorialsPoint - Pyhton3]
------

### Basic
+ Basic Syntax
+ Variable Types
+ Basic Operators
+ Decision Making
+ Loops
+ Numbers
+ Strings
+ Lists
+ Tuples
+ Dictionary
+ Date & Time
+ Function
+ Modules
+ File I/O
+ Exceptions


### Advanced
+ Classes/Objects
+ Reg Expressions
+ CGI Programming
+ Database Access
+ Networking
+ Sending Email
+ Multithreading
+ XML Processing
+ GUI Programming
+ Further Extensions

[TutorialsPoint - Pyhton3]:https://www.tutorialspoint.com/python3





