SQL 課程大綱
======
`開發工具 + SQL 語法 + ORM 框架`

+ [git clone](https://gitlab.com/GammaRayStudio/Tutor/Document)

<br>


<br>
12312312
### Docker - MySQL
+ Docker : <https://www.docker.com/>

**document**
```
+ SQL-Doc/docker-mysql.md
```

<br>

### DBeaver
+ DBeaver : <https://dbeaver.io/>

<br>

### IntelliJ Plugin : Database Navigatior
+ IntelliJ : <https://www.jetbrains.com/idea/>
+ Database Navigator : <https://plugins.jetbrains.com/plugin/1800-database-navigator>


<br>


SQL 語法 `Structured Query Language`
------
### 基礎
`CRUD`

+ SQL 概述
+ SQL 命令
  + DDL - Data Definition Language
  + DML - Data Manipulation Language
  + DCL - Data Control Language
+ RDBMS - 關聯式資料庫
+ 資料庫正規化
  + 第一正規化 (1NF , Normal Form)
  + 第二正規化 (2NF , Normal Form)
  + 第三正規化 (3NF , Normal Form)
+ SQL 數據類型 
+ SQL 運算符
+ SQL 語法 (Syntax)

**參考大綱**
+ SQL 命令 : DDL / DML / DCL
  + DDL : 數據定義語言
    + CREATE
    + ALTER
    + DROP
    + DATABASE & TABLE
  + DML : 數據操作語言
    + INSERT
    + SELECT
    + UPDATE
    + DELETE
    + WHERE
    + AND / OR
    + LIKE
    + TOP
    + ORDER BY
    + GROUP BY
    + DISTINCT
    + SORT
  + DCL : 數據控制語言
    + GRANT
    + REVOKE
+ 進階指令
  + JOIN
    + INNER JOIN
    + LEFT JOIN
    + RIGHT JOIN
  + UNION
  + NULL VALUE
  + AS
  + INDEX
  + ALTER
  + VIEW
  + TRANSACTION
  + Temporary Table
  + SUB Query
  + SEQUENCES


**ref**
```
+ 條件查詢與資料排序
+ 資料型態與內建函式
+ 資料匯總與資料分組
+ 多資料表查詢
+ 子查詢
```

ORM 框架
------
+ Java JDBC
+ MyBatis
+ Hibernate








